
fn fuel_calc(distance: i64) -> i64 {
    distance * (distance + 1) / 2
}

fn try_height(heights: &Vec<i64>, target: i64) -> (i64, i64) {
    let lower = target-1;
    let higher = target+1;

    let fuel_l = heights.iter().map( |h| fuel_calc(i64::abs(*h - lower)) ).reduce(|acc, v| acc + v).unwrap();
    let fuel_t = heights.iter().map( |h| fuel_calc(i64::abs(*h - target)) ).reduce(|acc, v| acc + v).unwrap();
    let fuel_h = heights.iter().map( |h| fuel_calc(i64::abs(*h - higher)) ).reduce(|acc, v| acc + v).unwrap();

    if fuel_t <= fuel_l && fuel_t <= fuel_h {
        (target, fuel_t)
    } else if fuel_h <= fuel_t && fuel_h <= fuel_l {
        (higher, fuel_h)
    } else {
        (lower, fuel_h)
    }
}

fn process(input: &str) -> usize {
    let heights: Vec<_> = input.split(',').map(|i| i64::from_str_radix(i, 10).unwrap() ).collect();
    let avg = (heights.iter().sum::<i64>() as f32 / heights.len() as f32).round() as i64;
    let len = heights.len();

    let mut target = avg;
    for _ in 0..len {
        let (n_target, n_fuel) = try_height(&heights, target);
        if target == n_target {
            return n_fuel as usize;
        } else {
            target = n_target;
        }
    }

    0
}

fn main() {
    let output = process(include_str!("../input.txt"));
    println!("Answer is: {}", output);
}

#[cfg(test)]
mod tests {
    #[test]
    fn sample_test() {
        let output = super::process(include_str!("../sample.txt"));
        assert_eq!(output, 168);
    }
}
