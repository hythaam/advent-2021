use std::collections::HashMap;

fn printmap(map: &HashMap::<(i32, i32), i32>) {
    (0..10).for_each(|i| {
        println!("{}", (0..10).map(|j| {
            let value = map.get(&(i, j)).unwrap_or(&0);
            if *value == 0 {
                format!("\x1b[0;31m{}\x1b[0m", value)
            } else {
                format!("{}", value)
            }
        }).collect::<String>());
    });
    println!();
}

fn flash(map: &mut HashMap::<(i32, i32), i32>, p: &(i32, i32)) {

    let adjacent = [
        (p.0 - 1, p.1 - 1),
        (p.0 - 1, p.1 + 0),
        (p.0 - 1, p.1 + 1),
        (p.0 - 0, p.1 - 1),
        (p.0 - 0, p.1 + 1),
        (p.0 + 1, p.1 - 1),
        (p.0 + 1, p.1 + 0),
        (p.0 + 1, p.1 + 1),
    ];

    for point in adjacent {
        let energy = map.get(&point);
        if let Some(&e) = energy {
            let e = e + 1;
            map.insert(point, e);
            if e == 10 {
                flash(map, &point);
            }
        }
    }
}

fn process(input: &str) -> usize {

    let mut octopuses = HashMap::<(i32, i32), i32>::new();
    input.split("\n").enumerate().for_each(|(i, line)|{
        line.chars().enumerate().for_each(|(j, c)| {
            octopuses.insert((i as i32, j as i32), c.to_digit(10).unwrap() as i32);
        });
    });

    // printmap(&octopuses);

    let mut syncd = false;
    let mut iter = 0;
    while !syncd {
        iter = iter + 1;
        for (point, energy) in octopuses.clone().iter() {
            let new_e = energy + 1;
            octopuses.insert(*point, new_e);
        }

        for (point, energy) in octopuses.clone().iter() {
            if *energy > 9 {
                flash(&mut octopuses, point);
            }
        }

        let mut flashes = 0;
        for (point, energy) in octopuses.clone().iter() {
            if *energy > 9 {
                octopuses.insert(*point, 0);
                flashes = flashes + 1;
            }
        }
        // printmap(&octopuses);
        if flashes == 100 {
            break;
        }

    }
    iter
}

fn main() {
    let output = process(include_str!("../input.txt"));
    println!("P2 Answer is: {}", output);
}

#[cfg(test)]
mod tests {
    #[test]
    fn sample_test() {
        let output = super::process(include_str!("../sample.txt"));
        assert_eq!(output, 195);
    }
}
