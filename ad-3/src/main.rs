
fn find_rating(mut input: Vec<&str>, flag: bool) -> u32 {
    let mut bit_criteria = String::new();
    while input.len() > 1 {
        let next_criteria = format!("{}{}", bit_criteria, "1");
        let matches = input.iter().filter( |i| i.starts_with(&next_criteria) ).count();

        if matches * 2 == input.len() {
            bit_criteria = format!("{}{}", bit_criteria, if flag {"1"} else {"0"});
            input = input.into_iter().filter( |i| i.starts_with(&bit_criteria)).collect();

        } else if (matches * 2 > input.len()) ^ !flag { 
            bit_criteria = next_criteria;
            input = input.into_iter().filter( |i| i.starts_with(&bit_criteria)).collect();

        } else {
            bit_criteria = format!("{}{}", bit_criteria, "0");
            input = input.into_iter().filter( |i| i.starts_with(&bit_criteria)).collect();

        };
    }
    u32::from_str_radix(input.first().unwrap(), 2).unwrap()
}

fn main() {
    let input: Vec<&str> = include_str!("../input.txt").split("\n").collect();

    let oxy_rating = find_rating(input.clone(), true);
    let co_rating = find_rating(input.clone(), false);
    println!("OxyGen: {}, CO2 Scrub: {}", oxy_rating, co_rating);
    println!("Life Support power rating: {}", oxy_rating * co_rating);

}

