use std::collections::HashMap;

fn process(input: &str, time: u16) -> usize {
    let mut fish_map: HashMap<u8, u64> = input.split(',')
        .fold(HashMap::new(), |mut acc, value| {
            let age = u8::from_str_radix(value, 10).unwrap();
            let &num = acc.get(&age).unwrap_or(&0);
            acc.insert(age, num + 1);
            acc
        });
    
    // println!("Initial State: {:?}", fish_map.iter());

    for t in 0..time {
        fish_map = fish_map.into_iter()
            .map(|(age, count)| {
                match age {
                    0 => [(6, count), (8, count)],
                    n => [(n-1, count), (0, 0)]
                }
            })
            .flatten()
            .fold(HashMap::new(), |mut acc, (age, count)| {
                let &num = acc.get(&age).unwrap_or(&0);
                acc.insert(age, num + count);
                acc
            });

        // println!("After {:2} days: {:?}", t, fish_map.iter());
    }

    fish_map.values().into_iter().fold(0, |acc, &count| {
        acc + count as usize
    })
}

fn main() {
    let output = process(include_str!("../input.txt"), 256);
    println!("Answer is: {}", output);
}

#[cfg(test)]
mod tests {
    #[test]
    fn sample_test() {
        let output = super::process(include_str!("../sample.txt"), 18);
        assert_eq!(output, 26);

        let output = super::process(include_str!("../sample.txt"), 80);
        assert_eq!(output, 5934);

        let output = super::process(include_str!("../sample.txt"), 256);
        assert_eq!(output, 26984457539);
    }
}