fn process(input: &str) -> usize {
    let lines = input.split("\n");
    let mut error_value = 0;
    let mut scores: Vec<usize> = lines.into_iter().map(|line| {
        let mut tokens = Vec::new();
        for c in line.chars() {
            let outcome = match c {
                '(' => { tokens.push(c); None },
                '[' => { tokens.push(c); None },
                '{' => { tokens.push(c); None },
                '<' => { tokens.push(c); None },

                ')' => { if tokens.pop().unwrap() == '(' { None } else { Some(c) } },
                ']' => { if tokens.pop().unwrap() == '[' { None } else { Some(c) } },
                '}' => { if tokens.pop().unwrap() == '{' { None } else { Some(c) } },
                '>' => { if tokens.pop().unwrap() == '<' { None } else { Some(c) } },

                _ => panic!("Received invalid input {}", c)
            };
            if let Some(d) = outcome {
                error_value = error_value + match d {
                    ')' => 3,
                    ']' => 57,
                    '}' => 1197,
                    '>' => 25137,
                    _ => panic!("Received invalid input {}", d)
                };
                tokens.clear();
                break;
            };
        };

        tokens.reverse();
        tokens.iter().fold(0, |acc, c| {
            (acc * 5) + match c {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => panic!("Received invalid input {}", c)
            }
        })
    }).filter(|&v| v > 0).collect();
    scores.sort();
    scores[(scores.len() - 1) / 2]
}

fn main() {
    let output = process(include_str!("../input.txt"));
    println!("P2 Answer is: {}", output);
}

#[cfg(test)]
mod tests {
    #[test]
    fn sample_test() {
        let output = super::process(include_str!("../sample.txt"));
        assert_eq!(output, 288957);
    }
}
