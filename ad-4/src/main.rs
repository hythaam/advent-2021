use std::iter::Iterator;

#[derive(Copy, Clone, Debug)]
enum Tile {
    Flipped(u8),
    Unflipped(u8)
}

#[derive(Copy, Clone, Debug)]
struct Board {
    tiles: [[Tile; 5]; 5],
    victory_round: Option<u32>,
    final_score: Option<u32>
}
impl Board {
    fn from_str(value: &str) -> Self {
        let mut output = Self { tiles: [[Tile::Unflipped(0); 5]; 5], victory_round: None, final_score: None };
        value.split("\n")
            .enumerate()
            .for_each( |(i, v)| {
                v.split(" ")
                    .filter( |s| s.len() != 0 )
                    .enumerate()
                    .for_each(|(j, value)| {
                        let value = u8::from_str_radix(value, 10).unwrap();
                        output.tiles[i][j] = Tile::Unflipped(value);
                    });
            });
        output
    }
    fn play_num(&mut self, num: u8) {
        self.tiles.iter_mut().flatten().for_each(|t| {
            *t = match t {
                Tile::Unflipped(n) if *n == num => Tile::Flipped(num),
                _ => *t
            }
        })
    }
    fn check_win(&self) -> bool {
        let row_win = self.tiles.iter().any( |r| {
            r.iter().all(|t| if let Tile::Flipped(_) = t { true } else { false } )
        });
        let col_win = (0..5).any(|i| self.tiles.iter().map(|r| r[i]).all(|t| if let Tile::Flipped(_) = t { true } else { false } ));
        row_win || col_win
    }
    fn score(&self) -> u32 {
        self.tiles.iter().flatten().fold(0, |acc, t| {
            if let Tile::Unflipped(n) = t { acc + *n as u32 } else { acc }
        })
    }
}

fn main() {
    let input: Vec<&str> = include_str!("../input.txt").split("\n\n").collect();
    let numbers = input.first().unwrap().split(',').map(|v| u8::from_str_radix(v, 10).unwrap());
    let mut boards: Vec<Board> = input[1..].iter().copied().map(Board::from_str).collect();
    numbers.enumerate().for_each(|(i, n)| {
        boards.iter_mut().for_each(|b| {
            if let None = b.victory_round {
                b.play_num(n);
                if b.check_win() {
                    b.victory_round = u32::try_from(i).ok();
                    b.final_score = u32::try_from(b.score() * n as u32).ok();
                }
            }
        });
    });
    let first = boards.iter().reduce(|winner, new| {
        if winner.victory_round < new.victory_round { winner } else { new }
    }).unwrap();
    println!("First winner won in round {:?} with score {:?}", first.victory_round, first.final_score);

    let last = boards.iter().reduce(|loser, new| {
        if loser.victory_round > new.victory_round { loser } else { new }
    }).unwrap();
    println!("Last winner won in round {:?} with score {:?}", last.victory_round, last.final_score);
}
