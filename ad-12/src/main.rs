use std::collections::HashMap;

fn get_length_to_end(map: HashMap::<String, (Vec<String>, bool)>, node: String, mut revisit_flag: bool) -> Option<usize> {
    if node == "end" {
        Some(1)
    } else {

        let connections = map.get(&node);
        match connections {
            // if the node still exists
            Some((vec, visited)) => {

                // if the node is lowercase, remove it as a potential future node
                let mut new_map = map.clone();
                if node.to_ascii_lowercase() == node {
                    new_map.remove(&node);
                    if revisit_flag && node != "start".to_string() {
                        new_map.insert(node.clone(), (vec.clone(), true));
                    }
                };

                // this logic is getting way too weird, but I'm too lazy to fix
                // the underlying data structure
                if *visited && !revisit_flag {
                    None
                } else {

                    if *visited && revisit_flag {
                        revisit_flag = false;
                    }

                    // return the number of ending it has
                    Some(vec.iter().fold(0, |acc, n| {
                        match get_length_to_end(new_map.clone(), n.clone(), revisit_flag) {
                            Some(num) => acc + num,
                            None => acc
                        }
                    }))
                }
            },

            // the node has already been removed, this is a dead end
            None => {
                None
            }
        }
    }
}

fn process(input: &str) -> usize {

    let mut nodes = HashMap::<String, (Vec<String>, bool)>::new();
    input.split("\n").for_each(|line| {
        let split = line.split("-").map(|s| { s.to_string() }).collect::<Vec<String>>();
        let first = split[0].clone();
        let last = split[1].clone();
        match nodes.get_mut(&first) {
            Some((vec, _)) => { vec.push(last.clone()); },
            None => { nodes.insert(first.clone(), (vec![last.clone()], false)); },
        };
        match nodes.get_mut(&last) {
            Some((vec, _)) => { vec.push(first.clone()); },
            None => { nodes.insert(last, (vec![first.clone()], false)); },
        };
    });

    get_length_to_end(nodes, "start".to_string(), true).unwrap()
}

fn main() {
    let output = process(include_str!("../input.txt"));
    println!("P2 Answer is: {}", output);
}

#[cfg(test)]
mod tests {

    #[test]
    fn sample_test() {
        let output = super::process(include_str!("../sample.txt"));
        assert_eq!(output, 36);
    }

    #[test]
    fn sample2_test() {
        let output = super::process(include_str!("../sample2.txt"));
        assert_eq!(output, 103);
    }

    #[test]
    fn sample3_test() {
        let output = super::process(include_str!("../sample3.txt"));
        assert_eq!(output, 3509);
    }
}

