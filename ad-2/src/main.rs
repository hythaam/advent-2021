use std::fs::File;
use std::io::{prelude::*, BufReader};

use std::str::FromStr;
use std::num::ParseIntError;
use std::result::Result;

#[derive(Debug, Copy, Clone, PartialEq)]
struct Command {
    depth: i32,
    forward: i32
}

impl FromStr for Command {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(" ").collect();
        let value = parts[1].parse::<i32>()?;
        match parts[0] {
            "forward" => Result::Ok(Self { depth: 0, forward: value }),
            "up" => Result::Ok(Self { depth: -value, forward: 0 }),
            "down" => Result::Ok(Self { depth: value, forward: 0 }),
            _ => Result::Ok(Self { depth: 0, forward: 0 })
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Sub {
    aim: i32,
    depth: i32,
    forward: i32
}

impl Sub {
    fn execute(mut self, com: Command) -> Self {
        self.aim += com.depth;
        self.forward += com.forward;
        self.depth += com.forward * self.aim;
        self
    }
}

fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);

    let output = reader.lines()
        .map(|v| {
            str::parse::<Command>(&v.unwrap()).unwrap()
        })
        .fold(Sub {aim: 0, depth: 0, forward: 0}, Sub::execute);

    println!("Output: {}, {} - {}", output.depth, output.forward, output.depth * output.forward);
}
