use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
struct Point(i32, i32);

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
struct Line {
    start: Point,
    unit_vector: Point,
    count: i32
}

impl Line {
    fn from_str(src: &str) -> Self {
        let parts: Vec<i32> = src
            .split(" -> ")
            .map( |s| s.split(",") )
            .flatten()
            .map( |s| i32::from_str_radix(s, 10).unwrap() )
            .collect();

        let start = Point(parts[0], parts[1]);
        let end = Point(parts[2], parts[3]);
        let vector = Point(end.0 - start.0, end.1 - start.1);
        let count = std::cmp::max(vector.0.abs(), vector.1.abs() );
        let unit_vector = Point(vector.0.signum(), vector.1.signum());

        Line {
            start,
            unit_vector,
            count
        }
    }

    fn is_orthogonal(&self) -> bool {
        self.unit_vector.0 == 0 || self.unit_vector.1 == 0
    }

    fn get_tiles(&self) -> HashSet<Point> {
        (0..self.count + 1).map( |i| {
            Point(
                self.start.0 + (self.unit_vector.0 * i),
                self.start.1 + (self.unit_vector.1 * i),
            )
        }).collect()
    }

    // fn intersection(&self, other: &Self) -> Vec<Point> {
    //     let other_tiles: Vec<Point> = other.get_tiles().collect();
    //     self.get_tiles().filter( |p| {
    //         other_tiles.iter().contains(p)
    //     }).collect()
    // }
}

fn main() {
    let lines: Vec<Line> = include_str!("../input.txt").split("\n").map(Line::from_str).collect();
    let intersections = lines.into_iter()
        // .filter(Line::is_orthogonal)
        .map(|l| l.get_tiles())
        .map(HashSet::into_iter)
        .flatten()
        .fold(HashMap::<Point, u8>::new(), |mut acc, tile| {
            let &value = acc.get(&tile).unwrap_or(&0);
            acc.insert(tile, value + 1);
            acc
        })
        .into_iter()
        .filter( |(_, value)| *value > 1 )
        .count();

    println!("Intersections: {}", intersections);
}
