use std::collections::HashMap;
use std::collections::HashSet;

struct Map(HashMap<(i32, i32), i32>);
impl Map {
    fn from_str(src: &str) -> Self {

        let mut output = HashMap::new();
        src.split("\n")
            .enumerate()
            .map(|(i, l)| {
                l.chars()
                    .enumerate()
                    .map(move |(j, c)| {
                        ((j as i32, i as i32), c.to_digit(10).unwrap() as i32)
                    })
            }).flatten()
            .for_each( |(p, d)| {
                output.insert(p, d);
            });

        Self( output )
    }

    fn is_minimum(&self, point: &(i32, i32)) -> bool {
        let l = self.0.get( &(point.0 - 1, point.1) ).unwrap_or(&i32::MAX);
        let r = self.0.get( &(point.0 + 1, point.1) ).unwrap_or(&i32::MAX);
        let u = self.0.get( &(point.0, point.1 - 1) ).unwrap_or(&i32::MAX);
        let d = self.0.get( &(point.0, point.1 + 1) ).unwrap_or(&i32::MAX);
        let point = self.0.get( &point ).unwrap();
        point < l && point < r && point < u && point < d
    }
}

fn try_adding_neighbors(map: &HashMap<(i32, i32), i32>, set: &mut HashSet<(i32, i32)>, point: (i32, i32)) {
    [
        (point.0 - 1, point.1),
        (point.0 + 1, point.1),
        (point.0, point.1 - 1),
        (point.0, point.1 + 1),
    ].iter().for_each(|p| {
        if *map.get(p).unwrap_or(&9) < 9 {
            if !set.contains(p) {
                set.insert(p.clone());
                try_adding_neighbors(map, set, p.clone());
            }
        }
    });
}

fn process(input: &str) -> usize {
    let mut basins = Vec::<HashSet<(i32, i32)>>::new();

    let mut map = HashMap::new();
    input.split("\n")
        .enumerate()
        .map(|(i, l)| {
            l.chars()
                .enumerate()
                .map(move |(j, c)| {
                    ((j as i32, i as i32), c.to_digit(10).unwrap() as i32)
                })
        }).flatten()
        .for_each( |(p, d)| {
            map.insert(p, d);
        });

    let mut remaining_points = map.clone();
    while !remaining_points.is_empty() {

        let (&current_point, _) = remaining_points.iter().next().unwrap();

        if *map.get(&current_point).unwrap_or(&9) < 9 {
            let mut set = HashSet::new();
            set.insert(current_point);
            try_adding_neighbors(&map, &mut set, current_point);
            for p in set.iter() {
                remaining_points.remove(p);
            }
            basins.push(set);
        };

        remaining_points.remove(&current_point);
    };

    basins.sort_by_key(|k| { k.len() });
    basins.reverse();
    basins[0].len() * basins[1].len() * basins[2].len()
}

fn main() {
    let output = process(include_str!("../input.txt"));
    println!("P1 Answer is: {}", output);
}

#[cfg(test)]
mod tests {
    #[test]
    fn sample_test() {
        let output = super::process(include_str!("../sample.txt"));
        assert_eq!(output, 1134);
    }
}
