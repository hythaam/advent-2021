use std::collections::HashMap;

fn sum_counts(map: &HashMap<char, u8>, s: &str) -> usize {
    s.chars().map( |c| {
        map.get(&c).unwrap_or(&0)
    }).sum::<u8>() as usize
}

fn sort_string(d: &str) -> String {
    let mut d: Vec<char> = d.chars().collect();
    d.sort();
    d.into_iter().collect()
}

fn process_line(digits: Vec<&str>, output: Vec<&str>) -> usize {

    let mut num_map = HashMap::<String, u8>::new();
    let mut seg_counts = HashMap::<char, u8>::new();

    digits.iter().map( |d| d.chars() )
        .flatten()
        .for_each(|c| {
            let &count = seg_counts.get(&c).unwrap_or(&0);
            seg_counts.insert(c, count + 1);
        });

    digits.iter().for_each(|&d| {
        let d = sort_string(d);
        let count = sum_counts(&seg_counts, &d);
        match count {
            17 => { num_map.insert(d, 1); },
            34 => { num_map.insert(d, 2); },
            39 => { num_map.insert(d, 3); },
            30 => { num_map.insert(d, 4); },
            37 => { num_map.insert(d, 5); },
            41 => { num_map.insert(d, 6); },
            25 => { num_map.insert(d, 7); },
            49 => { num_map.insert(d, 8); },
            45 => { num_map.insert(d, 9); },
            42 => { num_map.insert(d, 0); },
           _ => panic!("not a len at all!")
        }
    });

    output.iter().map( |s| {
        *num_map.get(&sort_string(s)).unwrap() as u64
    }).reduce(|acc, n| {
        acc * 10 + n
    }).unwrap() as usize
}

fn process(input: &str) -> usize {
    let inputs: Vec<_> = input.split("\n").collect();
    inputs.iter()
        .map( |&i| {
            let i: Vec<&str> = i.split(" | ").collect();
            process_line(i[0].split(" ").collect(), i[1].split(" ").collect())
        })
        .reduce(|acc, count| acc + count).unwrap()

    // 0
}

fn main() {
    let output = process(include_str!("../input.txt"));
    println!("Answer is: {}", output);
}

#[cfg(test)]
mod tests {
    #[test]
    fn sample_test() {
        let output = super::process(include_str!("../sample.txt"));
        assert_eq!(output, 61229);
    }
}
