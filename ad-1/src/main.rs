use std::fs::File;
use std::io::{prelude::*, BufReader};

use itertools::Itertools;

fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);

    // part one
    // let output = reader.lines()
    //     .map(|v| { str::parse::<u32>(&v.unwrap()).unwrap() })
    //     .tuple_windows::<(_, _)>()
    //     .fold(0, |value, pair| {
    //         if pair.1 > pair.0 {
    //             value + 1
    //         } else {
    //             value
    //         }
    
    // part two
    let output = reader.lines()
        .map(|v| { str::parse::<u32>(&v.unwrap()).unwrap() })
        .tuple_windows::<(_, _, _)>()
        .map(|window| {
            window.0 + window.1 + window.2
        })
        .tuple_windows::<(_, _)>()
        .fold(0, |value, pair| {
            if pair.1 > pair.0 {
                value + 1
            } else {
                value
            }
        });

    println!("Increasing pairs: {}", output);
}
